/// <reference types="@sveltejs/kit" />

// From https://www.npmjs.com/package/@poppanator/sveltekit-svg

declare module "*.svg?src" {
	const content: string
	export default content
}

declare module "*.svg?component" {
	const content: string
	export default content
}

declare module "*.svg?url" {
	const content: string
	export default content
}
