export interface Config {
	geoapifyApiKey: string
	mapboxAccessToken: string
}

export const config = {
	// TODO use env vars
	geoapifyApiKey: "36b182c3f4e144c397713cc3d0f6ce27",
	geoapifyGeocodeApiSearchBaseUrl: "https://api.geoapify.com/v1/geocode/search",
	geoapifyPlacesApiBaseUrl: "https://api.geoapify.com/v2/places",
	mapboxAccessToken:
		"pk.eyJ1IjoiZXhhbXBsZXMiLCJhIjoiY2p0MG01MXRqMW45cjQzb2R6b2ptc3J4MSJ9.zA2W0IkI0c6KaAhJfk9bWg",
}
