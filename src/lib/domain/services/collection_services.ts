import { derived, get } from "svelte/store"
import type { Readable } from "svelte/store"
import { combine, err, ok } from "neverthrow"
import type { Result, ResultAsync } from "neverthrow"
import type { LoadingManager } from "$lib/infra/svelte/stores/loading_manager"
import type { CollectionRepoStore } from "$lib/infra/svelte/stores/types"
import type { CollectionId } from "../entities/collections"
import type CollectionEntity from "../entities/collections"
import type CollectionFactory from "../entities/collection_factory"
import type { CreateCollectionOptions } from "../entities/collection_factory"
import type PlaceEntity from "../entities/place"
import type { PlaceTypeId } from "../entities/place_type"
import type CollectionRepo from "../repositories/collection_repo"
import type FindPlaceServices from "./find_place_services"
import type PlaceServices from "./place_services"
import type MapViewServices from "./map_view_services"
import type { PlaceId } from "../entities/place"
import { difference } from "$lib/array_utils"

export default class CollectionServices {
	constructor(
		private readonly collectionFactory: CollectionFactory,
		private readonly collectionRepoStore: CollectionRepoStore,
		private readonly findPlaceServices: FindPlaceServices,
		private readonly loadingManager: LoadingManager,
		private _placeServices: PlaceServices,
		private readonly mapViewServices: MapViewServices,
	) {}

	// Getter and setter for placeServices are defined to allow injecting _placeServices lazily because of a circular dependency between services.

	get placeServices(): PlaceServices {
		return this._placeServices
	}

	set placeServices(value: PlaceServices) {
		this._placeServices = value
	}

	private get collectionRepo(): CollectionRepo {
		return get(this.collectionRepoStore)
	}

	createCollection(options: CreateCollectionOptions): CollectionEntity {
		return this.collectionFactory.createCollection(options)
	}

	createCollectionWithPlacesAroundPlace(
		place: PlaceEntity,
		{
			radius,
			placeTypeIds,
		}: {
			radius: number
			placeTypeIds: PlaceTypeId[]
		},
	): ResultAsync<void, Error> {
		const placeId = place.id

		const placeTypesStr = placeTypeIds.join(",")
		const collectionId = `${placeTypesStr}-around-${placeId}`

		const work = this.findPlaceServices
			.findPlacesAroundPlace(place, { placeTypeIds, radius })
			.map((places) => {
				this.placeServices.savePlaces(places)
				const placeIds = places.map((place) => place.id)
				const collection = this.createCollection({
					id: collectionId,
					name: `Around ${place.name}`,
					placeIds,
					search: {
						boundary: {
							center: { type: "place", placeId },
							radius,
							type: "circle",
						},
						placeTypeIds,
					},
				})
				this.mapViewServices.gotoCollection(collectionId)
				this.saveCollection(collection)
			})

		return this.loadingManager.do(`collection:${collectionId}`, work)
	}

	findCollectionById(collectionId: CollectionId): Result<CollectionEntity, Error> {
		return this.collectionRepo.findCollectionById(collectionId)
	}

	listCollectionsAsStore(): Readable<CollectionEntity[]> {
		return derived(this.collectionRepoStore, (collectionRepo) => [...collectionRepo.list()])
	}

	listOtherCollections(collection: CollectionEntity): Iterable<CollectionEntity> {
		const collections = this.collectionRepo.list()
		return [...collections].filter((c) => c.id !== collection.id)
	}

	saveCollection(collection: CollectionEntity): void {
		this.collectionRepoStore.save(collection)
	}

	saveCollections(collections: CollectionEntity[]): void {
		for (const collection of collections) {
			this.saveCollection(collection)
		}
	}

	updateCollectionsAnchoredToPlace(placeId: PlaceId): Result<void, Error> {
		const { placeServices, collectionRepo } = this
		return combine([
			placeServices.findPlaceById(placeId),
			collectionRepo.findCollectionsAnchoredToPlace(placeId),
		]).map((result) => {
			const [place, collections] = result as [PlaceEntity, CollectionEntity[]]
			for (const collection of collections) {
				this.updateCollectionAnchoredToPlace(collection, place)
			}
		})
	}

	private updateCollectionAnchoredToPlace(
		collection: CollectionEntity,
		place: PlaceEntity,
	): Result<null, Error> {
		const { search } = collection
		if (search === null) {
			return err(new Error("Collection has no search"))
		}
		const { placeTypeIds, boundary } = search
		if (boundary.type === "circle") {
			const { radius } = boundary
			const currentPlaceIds = collection.placeIds
			this.createCollectionWithPlacesAroundPlace(place, { placeTypeIds, radius }).andThen(() =>
				this.findCollectionById(collection.id).andThen((collection2) => {
					const newPlaceIds = collection2.placeIds
					const obsoletePlaceIds = difference(currentPlaceIds, newPlaceIds)
					return this.placeServices.deletePlaces(obsoletePlaceIds)
				}),
			)
		}
		return ok(null)
	}
}
