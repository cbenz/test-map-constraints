import type { ResultAsync } from "neverthrow"
import type PlaceEntity from "../entities/place"
import type { PlaceTypeId } from "../entities/place_type"
import type Position from "../value_objects/position"

export default interface FindPlaceServices {
	findPlacesAroundPlace(
		place: PlaceEntity,
		options: { radius: number; placeTypeIds: PlaceTypeId[] },
	): ResultAsync<PlaceEntity[], Error>

	findPlacesByTypeInCircle(options: {
		center: Position
		radius: number
		placeTypeIds: PlaceTypeId[]
	}): ResultAsync<PlaceEntity[], Error>
}
