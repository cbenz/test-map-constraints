export type Uuid = string

export interface UuidServices {
	generateUuid(): Uuid
}
