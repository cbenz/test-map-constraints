import type { ResultAsync } from "neverthrow"
import type Position from "../value_objects/position"

export interface AutocompleterSuggestion {
	position: Position
	text: string
}

export default interface GeocodeServices {
	autocomplete(text: string): ResultAsync<AutocompleterSuggestion[], Error>

	geocode(text: string): ResultAsync<AutocompleterSuggestion | null, Error>
}
