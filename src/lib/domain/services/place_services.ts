import { err, ok, errAsync, okAsync, Result, ResultAsync } from "neverthrow"
import { derived, get } from "svelte/store"
import type { Readable } from "svelte/store"
import type CollectionEntity from "$lib/domain/entities/collections"
import type { CircleCenter } from "$lib/domain/entities/collections"
import type PlaceEntity from "../entities/place"
import { replace } from "$lib/immutable"
import type { PlaceId } from "../entities/place"
import type Position from "../value_objects/position"
import type PlaceRepo from "../repositories/place_repo"
import type { PlaceRepoStore } from "$lib/infra/svelte/stores/types"
import type PlaceFactory from "../entities/place_factory"
import type { CreatePlaceOptions } from "../entities/place_factory"
import type GeocodeServices from "./geocode_services"
import type CollectionServices from "./collection_services"

export default class PlaceServices {
	constructor(
		private readonly collectionServices: CollectionServices,
		private readonly geocodeServices: GeocodeServices,
		private readonly placeFactory: PlaceFactory,
		private readonly placeRepoStore: PlaceRepoStore,
	) {}

	private get placeRepo(): PlaceRepo {
		return get(this.placeRepoStore)
	}

	createCollectionForAllPlacesAsStore(): Readable<CollectionEntity> {
		const { collectionServices, placeRepoStore } = this
		return derived(placeRepoStore, (placeRepo) =>
			collectionServices.createCollection({
				color: null,
				id: "_all",
				name: "All places",
				placeIds: Array.from(placeRepo.list(), (place) => place.id),
				readOnly: true,
			}),
		)
	}

	createPlace(options: CreatePlaceOptions): PlaceEntity {
		return this.placeFactory.createPlace(options)
	}

	deletePlaces(placeIds: PlaceId[]): Result<null, Error> {
		let result: Result<null, Error> = ok(null)
		this.placeRepoStore.update((placeRepo) => {
			result = placeRepo.deletePlaces(placeIds)
			return placeRepo
		})
		return result
	}

	findPlaceById(placeId: PlaceId): Result<PlaceEntity, Error> {
		return this.placeRepo.findPlaceById(placeId)
	}

	findPlacesByIds(placeIds: PlaceId[]): Result<PlaceEntity[], Error> {
		return this.placeRepo.findPlacesByIds(placeIds)
	}

	findPlacesByIdsAsStore(placeIds: PlaceId[]): Readable<Result<PlaceEntity[], Error>> {
		return derived(this.placeRepoStore, (placeRepo) => placeRepo.findPlacesByIds(placeIds))
	}

	geocodePlace(placeId: PlaceId): ResultAsync<null, Error> {
		return this.findPlaceById(placeId).asyncAndThen((place) => {
			const { address } = place
			if (address === null) {
				return errAsync(new Error("Place has no address"))
			}
			return this.geocodeServices.geocode(address).andThen((suggestion) => {
				if (suggestion === null) {
					return errAsync(new Error("Geocoder found no suggestion for address"))
				}
				const place2 = replace(place, { position: suggestion.position })
				this.savePlace(place2)
				return okAsync(null)
			})
		})
	}

	// TODO
	// getCircleCollectionBoundary(
	// 	collection: CollectionEntity,
	// ): Result<CircleCollectionBoundary, Error> {
	// 	const { search } = collection
	// 	if (search === null) {
	// 		return err(new Error("Collection search is null"))
	// 	}
	// 	const { boundary } = search
	// 	if (boundary.type !== "circle") {
	// 		return err(new Error("Collection boundary type is not circle"))
	// 	}
	// 	return ok(boundary)
	// }

	getCircleCollectionBoundaryPosition(center: CircleCenter): Result<Position, Error> {
		const { type } = center
		// TODO replace with polymorphism
		if (type === "position") {
			return ok(center.position)
		} else if (type === "place") {
			// TODO ensure place has a position
			return this.findPlaceById(center.placeId).map((place) => place.position)
		}
		return err(new TypeError(type))
	}

	savePlace(place: PlaceEntity): void {
		this.placeRepoStore.save(place)
	}

	savePlaces(places: PlaceEntity[]): void {
		for (const place of places) {
			this.savePlace(place)
		}
	}

	// TODO
	// updateCollectionRadius(collection: CollectionEntity, radius: number): Result<null, Error> {
	// 	const boundary = this.getCircleCollectionBoundary(collection)

	//     const boundary =
	// 	return this.findPlaceById(placeId).andThen((place) =>
	// 		this.updateCollectionAnchoredToPlace(collection, place),
	// 	)
	// }
}
