export type Color = string

export type ColorPalette = readonly Color[]

export interface ColorServices {
	generateColor(): Color
}
