import type { MapViewStrategyStore } from "$lib/infra/svelte/stores/map_view_strategy_store"
import type { CollectionId } from "../entities/collections"
import type { PlaceId } from "../entities/place"
import type {
	CollectionMapViewStrategyFactory,
	PlaceMapViewStrategyFactory,
} from "$lib/infra/maplibre/map_view_strategy"

export default class MapViewServices {
	constructor(
		private readonly createCollectionMapViewStrategy: CollectionMapViewStrategyFactory,
		private readonly createPlaceMapViewStrategy: PlaceMapViewStrategyFactory,
		private readonly mapViewStrategyStore: MapViewStrategyStore,
	) {}

	gotoCollection(collectionId: CollectionId): void {
		const mapViewStrategy = this.createCollectionMapViewStrategy(collectionId)
		this.mapViewStrategyStore.set(mapViewStrategy)
	}

	gotoPlace(placeId: PlaceId): void {
		const mapViewStrategy = this.createPlaceMapViewStrategy(placeId)
		this.mapViewStrategyStore.set(mapViewStrategy)
	}
}
