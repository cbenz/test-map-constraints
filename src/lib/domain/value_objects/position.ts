export default class Position {
	constructor(public readonly lng: number, public readonly lat: number) {}

	static create({ lng, lat }: { lng: number; lat: number }): Position {
		return new Position(lng, lat)
	}
}
