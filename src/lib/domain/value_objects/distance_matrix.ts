import { err, ok, Result } from "neverthrow"
import type { PlaceId } from "../entities/place"
import type PlaceEntity from "../entities/place"

export type DistanceInMeters = number
type Key = string
export type Distances = Map<Key, DistanceInMeters>

export default class DistanceMatrixVO {
	constructor(
		public readonly placeIds1: PlaceId[],
		public readonly placeIds2: PlaceId[],
		private readonly distances: Distances,
	) {
		if (placeIds1.length === 0 || placeIds2.length === 0) {
			throw new Error("Place IDs must not be empty")
		}
	}

	static fromPlaceAgainstPlaces(
		place: PlaceEntity,
		places: PlaceEntity[],
	): Result<DistanceMatrixVO, Error> {
		return DistanceMatrixVO.fromPlaceAgainstOther([place], places)
	}

	static fromPlaceAgainstOther(
		places1: PlaceEntity[],
		places2: PlaceEntity[],
	): Result<DistanceMatrixVO, Error> {
		if (places1.length === 0) {
			return err(new Error("places1 must not be empty"))
		}
		if (places2.length === 0) {
			return err(new Error("places2 must not be empty"))
		}

		const distances: Distances = new Map()
		const placeIds1 = getIds(places1)
		const placeIds2 = getIds(places2)
		const distanceMatrix = new DistanceMatrixVO(placeIds1, placeIds2, distances)

		for (const place1 of places1) {
			for (const place2 of places2) {
				if (place1.position === null || place2.position === null) {
					continue
				}
				if (place1.id === place2.id) {
					distanceMatrix.setDistanceBetweenPlaces(place1.id, place2.id, 0)
				} else {
					const distanceResult = place1.distanceTo(place2)
					if (distanceResult.isErr()) {
						return err(distanceResult.error)
					}
					const d = distanceResult.value * 1000
					distanceMatrix.setDistanceBetweenPlaces(place1.id, place2.id, d)
				}
			}
		}

		return ok(distanceMatrix)
	}

	static fromPlaces(places: PlaceEntity[]): Result<DistanceMatrixVO, Error> {
		if (places.length === 0) {
			return err(new Error("places must not be empty"))
		}
		return DistanceMatrixVO.fromPlaceAgainstOther(places, places)
	}

	getDistanceBetweenPlaces(placeId1: PlaceId, placeId2: PlaceId): DistanceInMeters | null {
		;[placeId1, placeId2] = reorderPlaceIds(placeId1, placeId2)
		return this.distances.get(JSON.stringify([placeId1, placeId2])) ?? null
	}

	getClosestPlaces(): [[PlaceId, PlaceId], DistanceInMeters] {
		let closest: [PlaceId, PlaceId] | null = null
		let minDistance: DistanceInMeters = 0
		for (const placeId1 of this.placeIds1) {
			for (const placeId2 of this.placeIds2) {
				if (placeId1 === placeId2) {
					continue
				}
				const d = this.getDistanceBetweenPlaces(placeId1, placeId2)
				if (closest === null || d < minDistance) {
					closest = [placeId1, placeId2]
					minDistance = d
				}
			}
		}
		return [reorderPlaceIds(closest[0], closest[1]), minDistance]
	}

	setDistanceBetweenPlaces(placeId1: PlaceId, placeId2: PlaceId, distance: DistanceInMeters): void {
		;[placeId1, placeId2] = reorderPlaceIds(placeId1, placeId2)
		this.distances.set(JSON.stringify([placeId1, placeId2]), distance)
	}
}

function getIds(places: PlaceEntity[]): PlaceId[] {
	return places.map((place) => place.id)
}

export function reorderPlaceIds(placeId1: PlaceId, placeId2: PlaceId): [PlaceId, PlaceId] {
	return placeId1 < placeId2 ? [placeId1, placeId2] : [placeId2, placeId1]
}
