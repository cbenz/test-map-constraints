import { immerable } from "immer"
import type { AutocompleterSuggestion } from "../services/geocode_services"
import { replace } from "$lib/immutable"
import * as turf from "@turf/turf"
import { err, ok, Result } from "neverthrow"
import type Position from "../value_objects/position"

export type PlaceId = string

// TODO rename to Place
export default class PlaceEntity {
	[immerable] = true

	constructor(
		public readonly id: PlaceId,
		public readonly address: string | null,
		public readonly name: string,
		public readonly position: Position | null,
		public readonly draggable: boolean,
		public readonly visible: boolean,
	) {}

	// TODO move in infra-level service
	distanceTo(other: PlaceEntity): Result<number, Error> {
		if (this.position === null) {
			return err(new Error("This instance has no position"))
		}
		if (other.position === null) {
			return err(new Error("The other instance has no position"))
		}
		const distance = turf.distance(
			// TODO use mappers
			[this.position.lng, this.position.lat],
			[other.position.lng, other.position.lat],
		)
		return ok(distance)
	}

	updateFromAutocompleterSuggestion(suggestion: AutocompleterSuggestion): this {
		return replace(this, {
			address: suggestion.text,
			draggable: false,
			position: suggestion.position,
		})
	}
}
