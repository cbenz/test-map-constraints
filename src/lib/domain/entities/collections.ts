import { immerable } from "immer"
import type { PlaceId } from "./place"
import type Position from "../value_objects/position"
import type { PlaceTypeId } from "./place_type"

export type CollectionId = string

export interface PositionCircleCenter {
	position: Position
	type: "position"
}

export interface PlaceCircleCenter {
	placeId: PlaceId
	type: "place"
}

export type CircleCenter = PositionCircleCenter | PlaceCircleCenter

export interface CircleCollectionBoundary {
	center: CircleCenter
	radius: number
	type: "circle"
}

export type CollectionBoundary = CircleCollectionBoundary // or RectangleCollectionBoundary, PolygonCollectionBoundary...

export interface CollectionSearch {
	boundary: CollectionBoundary
	placeTypeIds: PlaceTypeId[]
}

// TODO rename to Collection
export default class CollectionEntity {
	[immerable] = true

	constructor(
		public readonly id: CollectionId,
		public readonly color: string | null,
		public readonly name: string,
		public readonly readOnly: boolean,
		public readonly placeIds: PlaceId[],
		public readonly search: CollectionSearch | null,
		public readonly visible: boolean,
	) {}

	isAnchoredToPlace(placeId: PlaceId): boolean {
		const { search } = this
		if (search === null) {
			return false
		}
		const { boundary } = search
		if (boundary.type === "circle") {
			const { center } = boundary
			return center.type === "place" && center.placeId === placeId
		}
		return false
	}
}
