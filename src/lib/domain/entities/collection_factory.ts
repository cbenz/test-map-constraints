import type { ColorServices } from "../services/color_services"
import type { UuidServices } from "../services/uuid_services"
import CollectionEntity from "./collections"
import type { CollectionId, CollectionSearch } from "./collections"
import type { PlaceId } from "./place"

export interface CreateCollectionOptions {
	color?: "generate" | string | null
	id?: CollectionId | null
	name: string
	placeIds?: PlaceId[]
	readOnly?: boolean
	search?: CollectionSearch | null
	visible?: boolean
}

export default class CollectionFactory {
	constructor(
		private readonly colorServices: ColorServices,
		private readonly uuidServices: UuidServices,
	) {}

	createCollection({
		color = "generate",
		id = null,
		name,
		placeIds = [],
		readOnly = false,
		search = null,
		visible = true,
	}: CreateCollectionOptions): CollectionEntity {
		if (id === null) {
			id = this.uuidServices.generateUuid()
		}
		if (color === "generate") {
			color = this.colorServices.generateColor()
		}
		return new CollectionEntity(id, color, name, readOnly, placeIds, search, visible)
	}
}
