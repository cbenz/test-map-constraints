export type PlaceTypeId = string

export default class PlaceType {
	constructor(public readonly id: PlaceTypeId, public readonly name: string) {}

	static create({ id, name }: { id: PlaceTypeId; name: string }): PlaceType {
		return new PlaceType(id, name)
	}
}
