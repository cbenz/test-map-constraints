import type { UuidServices } from "../services/uuid_services"
import PlaceEntity from "./place"
import type { PlaceId } from "./place"
import type Position from "../value_objects/position"

export interface CreatePlaceOptions {
	address?: string | null
	draggable?: boolean
	id?: PlaceId | null
	name?: string
	position?: Position | null
	visible?: boolean
}

export default class PlaceFactory {
	constructor(private readonly uuidServices: UuidServices) {}

	createPlace({
		address = null,
		draggable = false,
		id = null,
		name,
		position = null,
		visible = true,
	}: CreatePlaceOptions): PlaceEntity {
		if (id === null) {
			id = this.uuidServices.generateUuid()
		}
		if (name === undefined) {
			name = id
		}
		return new PlaceEntity(id, address, name, position, draggable, visible)
	}
}
