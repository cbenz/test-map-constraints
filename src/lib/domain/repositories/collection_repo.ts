import type { Result } from "neverthrow"
import type CollectionEntity from "../entities/collections"
import type { CollectionId } from "../entities/collections"
import type { PlaceId } from "../entities/place"

export default interface CollectionRepo {
	findCollectionById(collectionId: CollectionId): Result<CollectionEntity, Error>

	findCollectionsAnchoredToPlace(placeId: PlaceId): Result<CollectionEntity[], Error>

	list(): Iterable<CollectionEntity>

	save(collection: CollectionEntity): void
}
