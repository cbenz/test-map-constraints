import type { Result } from "neverthrow"
import type PlaceEntity from "../entities/place"
import type { PlaceId } from "../entities/place"

export default interface PlaceRepo {
	deletePlace(placeId: PlaceId): Result<null, Error>
	deletePlaces(placeIds: PlaceId[]): Result<null, Error>

	findPlaceById(placeId: PlaceId): Result<PlaceEntity, Error>
	findPlacesByIds(placeIds: PlaceId[]): Result<PlaceEntity[], Error>

	list(): Iterable<PlaceEntity>

	// TODO make async to have network implementation
	save(place: PlaceEntity): void
}
