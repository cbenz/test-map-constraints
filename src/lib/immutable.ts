import produce from "immer"

type Changes = Record<string, unknown>

export function replace<T>(instance: T, changes: Changes): T {
	return produce(instance, (draft) => {
		for (const [propertyName, value] of Object.entries(changes)) {
			draft[propertyName] = value
		}
	})
}
