import axios from "axios"
import { ResultAsync } from "neverthrow"
import queryString from "query-string"
import type { ParsedQuery } from "query-string"

export type Query = ParsedQuery

export async function fetchJsonUnsafe<T>(baseUrl: string, query: Query): Promise<T> {
	const url = queryString.stringifyUrl({ url: baseUrl, query })
	const response = await axios.get<T>(url)
	return response.data
}

export function fetchJson<T>(baseUrl: string, query: Query): ResultAsync<T, Error> {
	return ResultAsync.fromPromise(fetchJsonUnsafe(baseUrl, query), (error) => error as Error)
}
