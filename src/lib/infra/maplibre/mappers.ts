import type LngLat from "maplibre-gl/src/geo/lng_lat"
import Position from "$lib/domain/value_objects/position"

export function lngLatToPosition(lngLat: LngLat): Position {
	const { lng, lat } = lngLat
	return Position.create({ lng, lat })
}
