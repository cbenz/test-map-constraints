import type Position from "$lib/domain/value_objects/position"

export type PopupId = string

export interface PopupParams {
	id: PopupId
	position: Position
	text: string
}

export default class PopupController {
	constructor(private popups: Map<PopupId, PopupParams> = new Map()) {}

	addPopup(popup: PopupParams): void {
		this.popups.set(popup.id, popup)
	}

	listPopups(): PopupParams[] {
		return [...this.popups.values()]
	}

	removeAllPopups(): void {
		this.popups.clear()
	}

	removePopup(popupId: PopupId): void {
		const { popups } = this
		popups.delete(popupId)
	}
}
