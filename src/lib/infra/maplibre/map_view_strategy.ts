import type { Map as MaplibreMap } from "svelte-maplibre-gl"
import type { CollectionId } from "$lib/domain/entities/collections"
import type { PlaceId } from "$lib/domain/entities/place"
import type PlaceServices from "$lib/domain/services/place_services"
import type Position from "$lib/domain/value_objects/position"
import type CollectionServices from "$lib/domain/services/collection_services"
import type GeoJsonServices from "../geojson/geojson_services"

export default interface MapViewStrategy {
	apply(map: MaplibreMap): void
}

export class CenterZoomMapViewStrategy implements MapViewStrategy {
	constructor(private readonly center: Position, private readonly zoom: number) {}

	apply(map: MaplibreMap): void {
		const { center, zoom } = this
		// TODO parametrize speed in Map options
		map.easeTo({ center, speed: 2, zoom })
	}
}

export class CollectionMapViewStrategy implements MapViewStrategy {
	constructor(
		private readonly collectionId: CollectionId,
		private readonly collectionServices: CollectionServices,
		private readonly geoJsonServices: GeoJsonServices,
	) {}

	apply(map: MaplibreMap): void {
		const { collectionId, collectionServices, geoJsonServices } = this
		collectionServices.findCollectionById(collectionId).map((collection) => {
			geoJsonServices.getCollectionBoundingBox(collection).map((bbox) => {
				const padding = 30
				map.fitBounds(bbox, {
					padding,
					// TODO animate by default, but not on application load
					duration: 0,
				})
			})
		})
	}
}

// TODO use factory class
export type CollectionMapViewStrategyFactory = (
	collectionId: CollectionId,
) => CollectionMapViewStrategy

export class PlaceMapViewStrategy implements MapViewStrategy {
	constructor(private readonly placeId: PlaceId, private readonly placeServices: PlaceServices) {}

	apply(map: MaplibreMap): void {
		const { placeId, placeServices } = this
		placeServices.findPlaceById(placeId).map((place) => {
			// TODO parametrize speed
			map.easeTo({ center: place.position, speed: 2, zoom: 15 })
		})
	}
}

// TODO use factory class
export type PlaceMapViewStrategyFactory = (placeId: PlaceId) => PlaceMapViewStrategy
