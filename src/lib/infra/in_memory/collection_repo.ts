import { err, ok, Result } from "neverthrow"
import type CollectionEntity from "$lib/domain/entities/collections"
import type { CollectionId } from "$lib/domain/entities/collections"
import type CollectionRepo from "$lib/domain/repositories/collection_repo"
import type { PlaceId } from "$lib/domain/entities/place"

export default class InMemoryCollectionRepo implements CollectionRepo {
	constructor(private readonly collections: Map<CollectionId, CollectionEntity> = new Map()) {}

	findCollectionById(collectionId: CollectionId): Result<CollectionEntity, Error> {
		const collection = this.collections.get(collectionId)
		if (collection === undefined) {
			return err(new Error(`Collection "${collectionId}" not found`))
		}
		return ok(collection)
	}

	findCollectionsAnchoredToPlace(placeId: PlaceId): Result<CollectionEntity[], Error> {
		return ok([...this.list()].filter((collection) => collection.isAnchoredToPlace(placeId)))
	}

	list(): Iterable<CollectionEntity> {
		return this.collections.values()
	}

	save(collection: CollectionEntity): void {
		this.collections.set(collection.id, collection)
	}
}
