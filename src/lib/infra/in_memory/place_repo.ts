import { combine, err, ok, Result } from "neverthrow"
import type { PlaceId } from "$lib/domain/entities/place"
import type PlaceEntity from "$lib/domain/entities/place"
import type PlaceRepo from "$lib/domain/repositories/place_repo"

export default class InMemoryPlaceRepo implements PlaceRepo {
	constructor(private readonly places: Map<PlaceId, PlaceEntity> = new Map()) {}

	static create(places: PlaceEntity[] = []): InMemoryPlaceRepo {
		const placesMap = new Map()
		const repo = new InMemoryPlaceRepo(placesMap)
		for (const place of places) {
			repo.save(place)
		}
		return repo
	}

	deletePlace(placeId: PlaceId): Result<null, Error> {
		this.places.delete(placeId)
		return ok(null)
	}

	deletePlaces(placeIds: PlaceId[]): Result<null, Error> {
		return combine(placeIds.map((placeId) => this.deletePlace(placeId))).map(() => null)
	}

	findPlaceById(placeId: PlaceId): Result<PlaceEntity, Error> {
		const place = this.places.get(placeId)
		if (place === undefined) {
			// TODO return a domain error using a proper interface
			return err(new Error(`Place "${placeId}" not found`))
		}
		return ok(place)
	}

	findPlacesByIds(placeIds: PlaceId[]): Result<PlaceEntity[], Error> {
		return combine(placeIds.map((placeId) => this.findPlaceById(placeId)))
	}

	list(): Iterable<PlaceEntity> {
		return this.places.values()
	}

	save(place: PlaceEntity): void {
		this.places.set(place.id, place)
	}
}
