import PlaceType from "$lib/domain/entities/place_type"
import type CollectionServices from "$lib/domain/services/collection_services"
import type PlaceServices from "$lib/domain/services/place_services"
// TODO ensure import order with eslint plugin
import { okAsync, ResultAsync } from "neverthrow"

const homeId = "home"

export default class SampleDataServices {
	constructor(
		private readonly collectionServices: CollectionServices,
		private readonly placeServices: PlaceServices,
	) {}

	loadAllSampleData(): ResultAsync<void, Error> {
		return okAsync(this.loadSamplePlaces()).andThen(() => this.loadSampleCollections())
	}

	loadSamplePlaces(): void {
		const { placeServices } = this
		const home = placeServices.createPlace({
			id: homeId,
			address: "4 rue Rabelais, Vanves",
			name: "domicile",
		})
		placeServices.savePlace(home)
	}

	loadSampleCollections(): ResultAsync<void, Error> {
		const { collectionServices } = this
		const myCollection1 = collectionServices.createCollection({
			id: "my-collection-1",
			name: "My collection 1",
			placeIds: [homeId],
		})
		collectionServices.saveCollection(myCollection1)

		return this.createSupermarketCollectionAroundHome()
	}

	createSupermarketCollectionAroundHome(): ResultAsync<void, Error> {
		const { collectionServices, placeServices } = this
		return placeServices
			.geocodePlace(homeId)
			.andThen(() => placeServices.findPlaceById(homeId))
			.andThen((home2) =>
				collectionServices.createCollectionWithPlacesAroundPlace(home2, {
					radius: 500,
					// TODO
					placeTypeIds: ["supermarket"],
				}),
			)
	}

	loadSamplePlaceTypes(): void {
		const supermarket = PlaceType.create({ id: "supermarket", name: "Supermarket" })
		// TODO
	}
}
