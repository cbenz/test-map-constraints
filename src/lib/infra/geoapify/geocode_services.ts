import type { AutocompleterSuggestion } from "$lib/domain/services/geocode_services"
import type GeocodeServices from "$lib/domain/services/geocode_services"
import type { Query } from "../http"
import { fetchJson } from "../http"
import Position from "$lib/domain/value_objects/position"
import type { Feature } from "geojson"
import type { ResultAsync } from "neverthrow"

export default class GeoapifyGeocodeServices implements GeocodeServices {
	constructor(private readonly apiKey: string, private readonly baseUrl: string) {}

	autocomplete(text: string): ResultAsync<AutocompleterSuggestion[], Error> {
		return this.fetchFeatures(text).map((features) =>
			features.map((feature) => this.featureToAutocompleterSuggestion(feature)),
		)
	}

	geocode(text: string): ResultAsync<AutocompleterSuggestion | null, Error> {
		return this.fetchFeatures(text).map((features) =>
			this.featureToAutocompleterSuggestion(features[0]),
		)
	}

	private buildBaseQuery(): Query {
		return { apiKey: this.apiKey }
	}

	private featureToAutocompleterSuggestion(feature: Feature): AutocompleterSuggestion {
		const { coordinates } = feature.geometry
		const position = Position.create({ lng: coordinates[0], lat: coordinates[1] })
		const text = feature.properties.formatted
		return { position, text }
	}

	private fetchFeatures(text: string): ResultAsync<Feature[], Error> {
		const query = {
			...this.buildBaseQuery(),
			text,
		}
		return this.fetchJson(query).map((data) => {
			// TODO validate with schema
			return data.features ?? []
		})
	}

	private fetchJson(query: Query): ResultAsync<unknown, Error> {
		return fetchJson(this.baseUrl, query)
	}
}
