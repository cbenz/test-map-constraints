import type { Feature } from "geojson"
import { err, ok, Result, ResultAsync } from "neverthrow"
import type { PlaceId } from "$lib/domain/entities/place"
import type { PlaceTypeId } from "$lib/domain/entities/place_type"
import { fetchJson } from "../http"
import type { Query } from "../http"
import type FindPlaceServices from "$lib/domain/services/find_place_services"
import type PlaceEntity from "$lib/domain/entities/place"
import type GeoJsonMappers from "../geojson/geojson_mappers"
import type Position from "$lib/domain/value_objects/position"

type Category = string

// TODO load from config
const categoryMap: Map<PlaceId, Category> = new Map([["supermarket", "commercial.supermarket"]])

export default class GeoapifyFindPlaceServices implements FindPlaceServices {
	constructor(
		private readonly apiKey: string,
		private readonly baseUrl: string,
		private readonly geoJsonMappers: GeoJsonMappers,
	) {}

	findPlacesAroundPlace(
		place: PlaceEntity,
		{ radius, placeTypeIds }: { radius: number; placeTypeIds: PlaceTypeId[] },
	): ResultAsync<PlaceEntity[], Error> {
		// TODO ensure place has position by polymorphism
		const center = place.position
		return this.findPlacesByTypeInCircle({ center, radius, placeTypeIds })
	}

	findPlacesByTypeInCircle({
		center,
		radius,
		placeTypeIds,
	}: {
		center: Position
		radius: number
		placeTypeIds: PlaceTypeId[]
	}): ResultAsync<PlaceEntity[], Error> {
		const bias = this.buildBias(center)
		const filter = this.buildFilter(center, radius)
		const [categories, errors] = this.getCategoriesFromPlaceTypes(placeTypeIds)
		if (errors.length > 0) {
			// TODO
			console.error(errors)
		}
		const query = {
			...this.buildBaseQuery(),
			bias,
			categories: categories.join(","),
			filter,
		}
		return this.fetchJson(query).map((data) => {
			// TODO validate with schema
			const features: Feature[] = data.features ?? []
			const placeEntities = features.map((feature) => this.geoJsonMappers.featureToPlace(feature))
			return placeEntities
		})
	}

	getCategoriesFromPlaceTypes(placeTypeIds: PlaceTypeId[]): [Category[], Error[]] {
		const categories: Category[] = []
		const errors: Error[] = []
		// TODO extract as "partition" combinator
		for (const placeTypeId of placeTypeIds) {
			this.getCategoryFromPlaceType(placeTypeId).match(
				(ok) => {
					categories.push(ok)
				},
				(err) => {
					errors.push(err)
				},
			)
		}
		return [categories, errors]
	}

	getCategoryFromPlaceType(placeTypeId: PlaceTypeId): Result<Category, Error> {
		const category = categoryMap.get(placeTypeId)
		if (category === undefined) {
			return err(new Error(`No category for placeTypeId "${placeTypeId}"`))
		}
		return ok(category)
	}

	private buildBaseQuery(): Query {
		return { apiKey: this.apiKey }
	}

	private buildBias(center: Position): string {
		return `proximity:${center.lng},${center.lat}`
	}

	private buildFilter(center: Position, radius: number): string {
		return `circle:${center.lng},${center.lat},${radius}`
	}

	private fetchJson(query: Query): ResultAsync<unknown, Error> {
		return fetchJson(this.baseUrl, query)
	}
}
