import type { Color, ColorPalette, ColorServices } from "$lib/domain/services/color_services"

export default class ColorGenerator implements ColorServices {
	constructor(private readonly colorPalette: ColorPalette, private nextColorIndex = 0) {}

	generateColor(): Color {
		const { colorPalette, nextColorIndex } = this
		const color = colorPalette[nextColorIndex]
		this.nextColorIndex = (nextColorIndex + 1) % colorPalette.length
		return color
	}
}
