import { nanoid } from "nanoid"
import type { Uuid, UuidServices } from "$lib/domain/services/uuid_services"

export default class NanoidGenerator implements UuidServices {
	constructor(private readonly size: number) {}

	generateUuid(): Uuid {
		return nanoid(this.size)
	}
}
