import type { Writable } from "svelte/store"

export type WorkCallback<T> = (this: T) => void

export interface MutableClassStore<T> extends Writable<T> {
	do: (work: WorkCallback<T>) => void
}

export default function createMutableClassStore<T>(store: Writable<T>): MutableClassStore<T> {
	const { update } = store

	return {
		...store,
		do(work: WorkCallback<T>) {
			update((value) => {
				work.apply(value)
				return value
			})
		},
	}
}
