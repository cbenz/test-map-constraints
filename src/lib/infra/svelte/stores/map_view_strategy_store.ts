import { writable } from "svelte/store"
import type { Writable } from "svelte/store"
import type MapViewStrategy from "$lib/infra/maplibre/map_view_strategy"

export type MapViewStrategyStore = Writable<MapViewStrategy | null>

export default function createMapViewStrategyStore(): MapViewStrategyStore {
	return writable(null)
}
