import type CollectionEntity from "$lib/domain/entities/collections"
import type CollectionRepo from "$lib/domain/repositories/collection_repo"
import type PlaceEntity from "$lib/domain/entities/place"
import type PlaceRepo from "$lib/domain/repositories/place_repo"
import type { RepoStore } from "./repo_store"

export type CollectionRepoStore = RepoStore<CollectionEntity, CollectionRepo>

export type PlaceRepoStore = RepoStore<PlaceEntity, PlaceRepo>
