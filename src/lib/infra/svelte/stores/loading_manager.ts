import { derived, writable } from "svelte/store"
import type { Readable } from "svelte/store"
import type { ResultAsync } from "neverthrow"

export interface LoadingManager {
	do(key: string, work: ResultAsync<void, Error>): ResultAsync<void, Error>
	isLoading(key?: string | undefined): Readable<boolean>
}

export default function createLoadingManager(): LoadingManager {
	const loadingSetStore = writable(new Set<string>())
	return {
		do(key: string, work: ResultAsync<void, Error>): ResultAsync<void, Error> {
			loadingSetStore.update((loadingSet) => loadingSet.add(key))
			return work.map(() => {
				loadingSetStore.update((loadingSet) => {
					loadingSet.delete(key)
					return loadingSet
				})
			})
		},
		isLoading(key?: string | undefined): Readable<boolean> {
			return derived(loadingSetStore, (loadingSet) => {
				if (key === undefined) {
					return loadingSet.size > 0
				} else {
					return loadingSet.has(key)
				}
			})
		},
	}
}
