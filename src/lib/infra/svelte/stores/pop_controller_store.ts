import { writable } from "svelte/store"
import type PopupController from "$lib/infra/maplibre/popup_controller"
import createMutableClassStore from "./mutable_class_store"
import type { MutableClassStore } from "./mutable_class_store"

export type PopupControllerStore = MutableClassStore<PopupController>

export default function createPopupControllerStore(
	popupController: PopupController,
): PopupControllerStore {
	return createMutableClassStore(writable(popupController))
}
