import { writable } from "svelte/store"
import type { Readable, Updater } from "svelte/store"

export interface RepoStore<E, R> extends Readable<R> {
	save(entity: E): void
	update(this: void, updater: Updater<R>): void
}

interface CanSave<E> {
	save(entity: E): void
}

export default function createRepoStore<E, R extends CanSave<E>>(repo: R): RepoStore<E, R> {
	const { subscribe, update } = writable(repo)
	return {
		save(entity: E): void {
			update((repo) => {
				repo.save(entity)
				return repo
			})
		},
		subscribe,
		update,
	}
}
