import type { AppInstances } from "$lib/bootstrap"
import { getContext } from "svelte"

export interface AppContext {
	getAppInstances(): AppInstances
}

export const appContextKey = {}

export function getAppInstancesFromContext(): AppInstances {
	return getContext<AppContext>(appContextKey).getAppInstances()
}
