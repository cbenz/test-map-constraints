import * as turf from "@turf/turf"
import type { BBox, Feature, FeatureCollection, Point } from "geojson"
import { err, ok, Result } from "neverthrow"
import { derived } from "svelte/store"
import type { Readable } from "svelte/store"
import type { CircleCenter, CollectionBoundary } from "$lib/domain/entities/collections"
import type PlaceServices from "$lib/domain/services/place_services"
import type GeoJsonMappers from "./geojson_mappers"
import type CollectionEntity from "$lib/domain/entities/collections"
import type { PlaceRepoStore } from "../svelte/stores/types"

export default class GeoJsonServices {
	constructor(
		private readonly placeRepoStore: PlaceRepoStore,
		private readonly placeServices: PlaceServices,
		private readonly geoJsonMappers: GeoJsonMappers,
	) {}

	getCircleCollectionBoundary(center: CircleCenter, radius: number): Result<Feature, Error> {
		const { geoJsonMappers, placeServices } = this
		return placeServices.getCircleCollectionBoundaryPosition(center).map((position) =>
			turf.circle(geoJsonMappers.positionToLngLatArray(position), radius, {
				units: "meters",
			}),
		)
	}

	getCollectionBoundary(boundary: CollectionBoundary): Result<Feature, Error> {
		const { type } = boundary
		// TODO replace with polymorphism
		if (type === "circle") {
			const { center, radius } = boundary
			return this.getCircleCollectionBoundary(center, radius)
		}
		return err(new TypeError(type))
	}

	getCollectionBoundingBox(collection: CollectionEntity): Result<BBox, Error> {
		const { geoJsonMappers, placeServices } = this
		const { search } = collection

		if (search === null) {
			return placeServices.findPlacesByIds(collection.placeIds).andThen((places) => {
				const placesWithPosition = places.filter((place) => place.position !== null)
				if (placesWithPosition.length === 0) {
					return err(new Error("Could not compute a bounding box from 0 points"))
				}
				const points = placesWithPosition.map((place) => {
					// TODO ensure place.position is not null (with correct polymorphism?)
					return turf.point(geoJsonMappers.positionToLngLatArray(place.position))
				})
				const featureCollection = turf.featureCollection(points)
				const bbox = turf.bbox(featureCollection)
				return ok(bbox)
			})
		}

		const { boundary } = search

		// TODO use polymorphism
		if (boundary.type === "circle") {
			return this.getCollectionBoundary(boundary).map(turf.bbox)
		}

		return err(new Error(`Unsupported boundary type: ${boundary.type}`))
	}

	getPlaces(collection: CollectionEntity): Result<FeatureCollection<Point>, Error> {
		const { geoJsonMappers, placeServices } = this
		return placeServices.findPlacesByIds(collection.placeIds).map((places) => {
			const points = places
				.filter((x) => x.position !== null && x.visible)
				.map((place): Feature<Point> => {
					const feature = turf.point(geoJsonMappers.positionToLngLatArray(place.position)) // TODO ensure place.position exists
					const { address, id, name, visible } = place
					feature.properties = { address, id, name, visible }
					return feature
				})
			return turf.featureCollection(points)
		})
	}

	getPlacesAsStore(
		collection: CollectionEntity,
	): Readable<Result<FeatureCollection<Point>, Error>> {
		return derived(this.placeRepoStore, () => this.getPlaces(collection))
	}
}
