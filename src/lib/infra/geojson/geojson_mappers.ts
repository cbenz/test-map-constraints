import type { Feature } from "geojson"
import type PlaceEntity from "$lib/domain/entities/place"
import Position from "$lib/domain/value_objects/position"
import type PlaceFactory from "$lib/domain/entities/place_factory"

export default class GeoJsonMappers {
	constructor(private readonly placeFactory: PlaceFactory) {}

	featureToPlace(feature: Feature): PlaceEntity {
		const { properties } = feature
		// TODO do not hardcode `geoapify:` prefix here
		const id = `geoapify:place_id=${properties.place_id}`
		const { name } = properties
		const { coordinates } = feature.geometry
		const position = Position.create({ lng: coordinates[0], lat: coordinates[1] })
		const address = properties.address_line2
		return this.placeFactory.createPlace({ id, name, position, address })
	}

	lngLatArrayToPosition(lngLat: number[]): Position {
		const [lng, lat] = lngLat
		return Position.create({ lng, lat })
	}

	positionToLngLatArray(position: Position): number[] {
		const { lng, lat } = position
		return [lng, lat]
	}
}
