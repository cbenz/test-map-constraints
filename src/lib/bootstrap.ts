import { schemeCategory10 } from "d3-scale-chromatic"
import { config } from "./config"
import GeoapifyFindPlaceServices from "./infra/geoapify/find_place_services"
import GeoapifyGeocodeServices from "./infra/geoapify/geocode_services"
import InMemoryCollectionRepo from "./infra/in_memory/collection_repo"
import InMemoryPlaceRepo from "./infra/in_memory/place_repo"
import createRepoStore from "./infra/svelte/stores/repo_store"
import createLoadingManager from "./infra/svelte/stores/loading_manager"
import type { LoadingManager } from "./infra/svelte/stores/loading_manager"
import createMapViewStrategyStore from "./infra/svelte/stores/map_view_strategy_store"
import type { MapViewStrategyStore } from "./infra/svelte/stores/map_view_strategy_store"
import { CollectionMapViewStrategy, PlaceMapViewStrategy } from "./infra/maplibre/map_view_strategy"
import type { PlaceId } from "./domain/entities/place"
import type { CollectionId } from "./domain/entities/collections"
import ColorGenerator from "./infra/color/color_generator"
import NanoidGenerator from "./infra/nanoid/nanoid_generator"
import PlaceServices from "./domain/services/place_services"
import CollectionFactory from "./domain/entities/collection_factory"
import PlaceFactory from "./domain/entities/place_factory"
import GeoJsonMappers from "./infra/geojson/geojson_mappers"
import PopupController from "./infra/maplibre/popup_controller"
import createPopupControllerStore from "./infra/svelte/stores/pop_controller_store"
import type { PopupControllerStore } from "./infra/svelte/stores/pop_controller_store"
import CollectionServices from "./domain/services/collection_services"
import MapViewServices from "./domain/services/map_view_services"
import GeoJsonServices from "./infra/geojson/geojson_services"
import type GeocodeServices from "./domain/services/geocode_services"
import SampleDataServices from "./infra/sample_data/sample_data_services"

export interface AppInstances {
	collectionFactory: CollectionFactory
	collectionServices: CollectionServices
	geocodeServices: GeocodeServices
	geoJsonMappers: GeoJsonMappers
	geoJsonServices: GeoJsonServices
	loadingManager: LoadingManager
	mapboxAccessToken: string
	mapViewServices: MapViewServices
	mapViewStrategyStore: MapViewStrategyStore
	placeServices: PlaceServices
	popupControllerStore: PopupControllerStore
	sampleDataServices: SampleDataServices
}

export function createAppInstances(): AppInstances {
	const {
		geoapifyApiKey,
		geoapifyGeocodeApiSearchBaseUrl,
		geoapifyPlacesApiBaseUrl,
		mapboxAccessToken,
	} = config
	const colorPalette = schemeCategory10
	const colorServices = new ColorGenerator(colorPalette)
	const nanoidGeneratorSize = 10 // TODO use config
	const uuidServices = new NanoidGenerator(nanoidGeneratorSize)
	const collectionFactory = new CollectionFactory(colorServices, uuidServices)
	const collectionRepo = new InMemoryCollectionRepo()
	const collectionRepoStore = createRepoStore(collectionRepo)
	const placeFactory = new PlaceFactory(uuidServices)
	const geoJsonMappers = new GeoJsonMappers(placeFactory)
	const findPlaceServices = new GeoapifyFindPlaceServices(
		geoapifyApiKey,
		geoapifyPlacesApiBaseUrl,
		geoJsonMappers,
	)
	const loadingManager = createLoadingManager()
	let placeServices = null as unknown as PlaceServices
	const createCollectionMapViewStrategy: (collectionId: CollectionId) => CollectionMapViewStrategy =
		(collectionId: CollectionId) =>
			new CollectionMapViewStrategy(collectionId, collectionServices, geoJsonServices)
	const mapViewStrategyStore = createMapViewStrategyStore()
	const createPlaceMapViewStrategy = (placeId: PlaceId) =>
		new PlaceMapViewStrategy(placeId, placeServices)
	const mapViewServices = new MapViewServices(
		createCollectionMapViewStrategy,
		createPlaceMapViewStrategy,
		mapViewStrategyStore,
	)
	const collectionServices = new CollectionServices(
		collectionFactory,
		collectionRepoStore,
		findPlaceServices,
		loadingManager,
		placeServices,
		mapViewServices,
	)
	const geocodeServices = new GeoapifyGeocodeServices(
		geoapifyApiKey,
		geoapifyGeocodeApiSearchBaseUrl,
	)
	const placeRepo = new InMemoryPlaceRepo()
	const placeRepoStore = createRepoStore(placeRepo)
	placeServices = new PlaceServices(
		collectionServices,
		geocodeServices,
		placeFactory,
		placeRepoStore,
	)
	collectionServices.placeServices = placeServices
	const geoJsonServices = new GeoJsonServices(placeRepoStore, placeServices, geoJsonMappers)
	const popupController = new PopupController()
	const popupControllerStore = createPopupControllerStore(popupController)
	const sampleDataServices = new SampleDataServices(collectionServices, placeServices)
	return {
		collectionFactory,
		collectionServices,
		geocodeServices,
		geoJsonMappers,
		geoJsonServices,
		loadingManager,
		mapboxAccessToken,
		mapViewServices,
		mapViewStrategyStore,
		placeServices,
		popupControllerStore,
		sampleDataServices,
	}
}
